#   CREATED BY JOSH BOWMAN IN PYTHON 3
#
#   Challenge found at
#       https://www.reddit.com/r/dailyprogrammer/comments/7s888w/20180122_challenge_348_easy_the_rabbit_problem/
#
#   THE RABBIT PROBLEM
#
#   NOTE: all times are in months

# method to age the rabbits each month
def ageRabbits(rab):
    pass


# method to birth rabbits every month
def birthRabbits(rab):
    pass

def main():

    MALE_BABIES_PER_MONTH = 5
    FEM_BABIES_PER_MONTH = 9
    FERTILE_AGE = 4
    DEATH_AGE = 96

    elapsed_months = 0

    init_arr = []

    while len(init_arr) != 3:

        print("ENTER INPUT (Male rabbits, Female rabbits, rabbits needed for world domination) :")

        init_arr = input().split(" ")

        # check input length for validity
        if len(init_arr) != 3:
            print("Invalid input, try again")

    # initial rabbit values from user
    male_rabbits = int(init_arr[0])
    female_rabbits = int(init_arr[1])
    rabbits_needed = int(init_arr[2])

    total_rabbits = male_rabbits + female_rabbits

    # list of rabbits, index is age
    rabbits = [0] * 97

    if female_rabbits == 0:
        print("They'll never take over with no mamas!")
        exit(1)

    # initial rabbits are 2 months old
    rabbits[2] = male_rabbits + female_rabbits

    while total_rabbits < rabbits_needed:
        birthRabbits(rabbits)

        ageRabbits(rabbits)

        elapsed_months += 1

if __name__ == '__main__':

    main()